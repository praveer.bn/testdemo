package TestDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Formatter;

public class PartTimeEmployeeCRUD_Operation {
    Connection connection;
    Statement statement;
    PreparedStatement preparedStatement;

    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));

    public void insertion(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            String sql="insert into ptemp (pid, phr,psalary_hr,psalary) values(?,?,?,?)";
            preparedStatement=connection.prepareStatement(sql);
            System.out.println("plz enter the same id again");
            int pid=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the hours");
            int phr=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the salary per hour");
            int psphr=Integer.parseInt(bufferedReader.readLine());
            int psalary=(phr*psphr);
            preparedStatement.setInt(1,pid);
            preparedStatement.setInt(2,phr);
            preparedStatement.setInt(3,psphr);
//            preparedStatement.setInt(4,fixedsalary);
            preparedStatement.setInt(4,psalary);
            preparedStatement.executeUpdate();
            System.out.println("*************employee data stored");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public void fetchRecord(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("select emp.eid,emp.ename,ptemp.phr,ptemp.psalary_hr,ptemp.psalary from emp" +
                    " inner join ptemp on emp.eid=ptemp.pid");
            Formatter formatter=new Formatter();

            formatter.format("%15s %15s %15s %15s %15s   \n","emp_id","emp_name","emp_workhr","emp_s/p","emp_salary");
            System.out.println("part time Empolyess details");
            while (resultSet.next()) {
                formatter.format("%15s %15s %15s %15s %15s  \n", resultSet.getString(1), resultSet.getString(2)
                        ,resultSet.getString(3),resultSet.getString(4),resultSet.getString(5));

            }
            System.out.println(formatter);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
    public void fetchRecordSalaryCondition(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery(" select emp.eid,emp.ename,ptemp.phr,ptemp.psalary_hr," +
                    "ptemp.psalary from emp  inner join ptemp on emp.eid=ptemp.pid where ptemp.psalary >= 1000");
            Formatter formatter=new Formatter();

            formatter.format("%15s %15s %15s %15s %15s   \n","emp_id","emp_name","emp_workhr","emp_s/p","emp_salary");
            System.out.println("part time Empolyess details whose salary is greater then 25000");
            while (resultSet.next()) {
                formatter.format("%15s %15s %15s %15s %15s  \n", resultSet.getString(1), resultSet.getString(2)
                        ,resultSet.getString(3),resultSet.getString(4),resultSet.getString(5));

            }
            System.out.println(formatter);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
