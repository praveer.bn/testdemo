package TestDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Employee_Menu {
    Employee employee=new Employee();
    FullTimeEmployeeCRUD_Operation fullTimeEmployeeCRUD_operation=new FullTimeEmployeeCRUD_Operation();
    PartTimeEmployeeCRUD_Operation partTimeEmployeeCRUD_operation=new PartTimeEmployeeCRUD_Operation();
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));

    boolean flag=true;
    public  void menu() throws IOException {

        while (flag){
            System.out.println("press 0 for exit");
            System.out.println("press 1 for insertion");
            System.out.println("press 2 for update name");
            System.out.println("press 3 for delete employee");
            System.out.println("press 4 record of full time emp");
            System.out.println("press 5 record of part time emp");
            System.out.println("press 6 salary condition for FTE grt then 1000");
            System.out.println("press 7 salary condition for PTE grt then 1000");
            int option =Integer.parseInt(bufferedReader.readLine());
            switch (option) {
                case 1:
                    employee.insertion();
                     break;
                case 2:
                    employee.EmployeeUpdate();
                    break;
                case 3:
                    employee.EmployeeDelete();
                    break;
                case 4:
                    fullTimeEmployeeCRUD_operation.fetchRecord();
                    break;
                case 5:
                    partTimeEmployeeCRUD_operation.fetchRecord();
                    break;
                case 6:
                    fullTimeEmployeeCRUD_operation.fetchRecordSalaryCondition();
                    break;
                case 7:
                    partTimeEmployeeCRUD_operation.fetchRecordSalaryCondition();
                    break;
                case 0:
                    flag=false;
                    break;
                default:
                    System.out.println("wrong input");

            }
        }

    }

    public static void main(String[] args) throws IOException {
        new Employee_Menu().menu();
    }
}
/*
"C:\Program Files\Java\jdk1.8.0_202\bin\java.exe" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\lib\idea_rt.jar=61844:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\bin" -Dfile.encoding=UTF-8 -classpath "C:\Program Files\Java\jdk1.8.0_202\jre\lib\charsets.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\deploy.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\access-bridge-64.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\cldrdata.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\dnsns.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\jaccess.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\jfxrt.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\localedata.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\nashorn.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\sunec.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\sunjce_provider.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\sunmscapi.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\sunpkcs11.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\ext\zipfs.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\javaws.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\jce.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\jfr.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\jfxswt.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\jsse.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\management-agent.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\plugin.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\resources.jar;C:\Program Files\Java\jdk1.8.0_202\jre\lib\rt.jar;C:\Users\coditas\IdeaProjects\TEST\out\production\TEST;C:\Users\coditas\Downloads\mysql-connector-java-8.0.28.jar" TestDemo.Employee_Menu
press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
4
Loading class `com.mysql.jdbc.Driver'. This is deprecated. The new driver class is `com.mysql.cj.jdbc.Driver'. The driver is automatically registered via the SPI and manual loading of the driver class is generally unnecessary.
full time Empolyess details
         emp_id        emp_name      emp_workhr         emp_s/p       emp_fixed      emp_salary
              2         praveer               2             100            1000            1200
              4              rt               2             100            2000            2200
              5              rt               2             100            2000            2200

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
5
part time Empolyess details
         emp_id        emp_name      emp_workhr         emp_s/p      emp_salary
              6          yojana               2             200             400

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
1
enter the name
sanyami
entered basic details in employee table
enter the type of employee :: 1:full time employee ,2:part time employee
1
plz enter the same id again
7
enter the hours
12
enter the salary per hour
100
enter the fixed salary
2000
*************employee data stored
press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
1
enter the name
aman
entered basic details in employee table
enter the type of employee :: 1:full time employee ,2:part time employee
2
plz enter the same id again
8
enter the hours
11
enter the salary per hour
155
*************employee data stored
press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
4
full time Empolyess details
         emp_id        emp_name      emp_workhr         emp_s/p       emp_fixed      emp_salary
              2         praveer               2             100            1000            1200
              4              rt               2             100            2000            2200
              5              rt               2             100            2000            2200
              7             knd              12             100            2000            3200

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
5
part time Empolyess details
         emp_id        emp_name      emp_workhr         emp_s/p      emp_salary
              6          yojana               2             200             400
              8         sanyami              11             155            1705

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
3
enter the id u wanna delete
5
******************* employee data deleted
press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
4
full time Empolyess details
         emp_id        emp_name      emp_workhr         emp_s/p       emp_fixed      emp_salary
              2         praveer               2             100            1000            1200
              4              rt               2             100            2000            2200
              7             knd              12             100            2000            3200

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
6
full time Empolyess details whose salary is greater then 25000
         emp_id        emp_name      emp_workhr         emp_s/p       emp_fixed      emp_salary
              2         praveer               2             100            1000            1200
              4              rt               2             100            2000            2200
              7             knd              12             100            2000            3200

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
7
part time Empolyess details whose salary is greater then 25000
         emp_id        emp_name      emp_workhr         emp_s/p      emp_salary
              8         sanyami              11             155            1705

press 0 for exit
press 1 for insertion
press 2 for update name
press 3 for delete employee
press 4 record of full time emp
press 5 record of part time emp
press 6 salary condition for FTE grt then 1000
press 7 salary condition for PTE grt then 1000
0

Process finished with exit code 0

 */
