package TestDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Formatter;

public class FullTimeEmployeeCRUD_Operation {
    Connection connection;
    Statement statement;
    PreparedStatement preparedStatement;
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));

    public void insertion(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            String sql="insert into ftemp (fid, fhr,salary_hr,fixed_s,fsalary) values(?,?,?,?,?)";
            preparedStatement=connection.prepareStatement(sql);
            System.out.println("plz enter the same id again");
            int fid=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the hours");
            int fhr=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the salary per hour");
            int fsphr=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the fixed salary");
            int fixedsalary=Integer.parseInt(bufferedReader.readLine());
            int salary=fixedsalary+(fhr*fsphr);
            preparedStatement.setInt(1,fid);
            preparedStatement.setInt(2,fhr);
            preparedStatement.setInt(3,fsphr);
            preparedStatement.setInt(4,fixedsalary);
            preparedStatement.setInt(5,salary);
            preparedStatement.executeUpdate();
            System.out.println("*************employee data stored");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void fetchRecord(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
           statement=connection.createStatement();
           ResultSet resultSet=statement.executeQuery("select emp.eid,emp.ename,ftemp.fhr,ftemp.salary_hr,ftemp.fixed_s,ftemp.fsalary from emp" +
                   " inner join ftemp on emp.eid=ftemp.fid");
            Formatter formatter=new Formatter();
            System.out.println("full time Empolyess details");

            formatter.format("%15s %15s %15s %15s %15s %15s  \n","emp_id","emp_name","emp_workhr","emp_s/p","emp_fixed","emp_salary");
            while (resultSet.next()) {
                formatter.format("%15s %15s %15s %15s %15s %15s \n", resultSet.getString(1), resultSet.getString(2)
                ,resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6));

            }
            System.out.println(formatter);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
    public void fetchRecordSalaryCondition(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("select emp.eid,emp.ename,ftemp.fhr,ftemp.salary_hr,ftemp.fixed_s,ftemp.fsalary from emp" +
                    " inner join ftemp on emp.eid=ftemp.fid where ftemp.fsalary >= 1000");
            Formatter formatter=new Formatter();
            System.out.println("full time Empolyess details whose salary is greater then 25000");

            formatter.format("%15s %15s %15s %15s %15s %15s  \n","emp_id","emp_name","emp_workhr","emp_s/p","emp_fixed","emp_salary");
            while (resultSet.next()) {
                formatter.format("%15s %15s %15s %15s %15s %15s \n", resultSet.getString(1), resultSet.getString(2)
                        ,resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6));

            }
            System.out.println(formatter);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
