package TestDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;

public class Employee {
    Connection connection;
    Statement statement;
    PreparedStatement preparedStatement;
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    FullTimeEmployeeCRUD_Operation fullTimeEmployeeCRUD_operation=new FullTimeEmployeeCRUD_Operation();
    PartTimeEmployeeCRUD_Operation partTimeEmployeeCRUD_operation=new PartTimeEmployeeCRUD_Operation();

    public void insertion(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            System.out.println("enter the name");
            String name=bufferedReader.readLine();
            preparedStatement=connection.prepareStatement("insert into emp (ename) values ('"+name+"');");
//            preparedStatement.setString(1,name);
            preparedStatement.executeUpdate();
            System.out.println("entered basic details in employee table");
            System.out.println("enter the type of employee :: 1:full time employee ,2:part time employee");
            int option =Integer.parseInt(bufferedReader.readLine());
            switch (option){
                case 1:
                    fullTimeEmployeeCRUD_operation.insertion();
                    break;
                case 2:
                    partTimeEmployeeCRUD_operation.insertion();
                    break;
                default:
                    System.out.println("wrong input");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void EmployeeUpdate(){
        try{
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
        System.out.println("enter the name u wanna update");
        String name=bufferedReader.readLine();
            System.out.println("enter the id");
            int id=Integer.parseInt(bufferedReader.readLine());
        preparedStatement=connection.prepareStatement("update emp set ename=? where eid=?");
            preparedStatement.setString(1,name);
            preparedStatement.setInt(2,id);
        preparedStatement.executeUpdate();
            System.out.println("******************* employee data upadted");

    } catch (ClassNotFoundException e) {
        throw new RuntimeException(e);
    } catch (SQLException e) {
        throw new RuntimeException(e);
    } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void EmployeeDelete(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");

            System.out.println("enter the id u wanna delete");
            int id=Integer.parseInt(bufferedReader.readLine());
            preparedStatement=connection.prepareStatement("delete from emp where eid=?");
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            System.out.println("******************* employee data deleted");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    }

